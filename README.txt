This module provides a hopefully simple way to restrict access to 
organic group content to subgroups of users. You first define the 
levels of access you need (for example: top secret, secret, confidential) 
and, using Drupal permissions system, assign permissions to view content 
of different levels to the different user roles. 

You can also give content editors the possibility to restrict node 
access to different levels.

This module is particularly useful in combination with OG User Roles.
